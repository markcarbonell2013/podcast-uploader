#! /usr/bin/env bash

PODCAST_UPLOADER_DIR="/home/neo/Documents/podcast/podcast-uploader"

cd "$PODCAST_UPLOADER_DIR"
node_modules/.bin/browserify view/main.js -o view/bundle.js
