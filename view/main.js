const WaveSurfer = require('../node_modules/wavesurfer.js/dist/wavesurfer.js')
const MicrophonePlugin = require('../node_modules/wavesurfer.js/dist/plugin/wavesurfer.microphone.min.js')
let wavesurfer

document.addEventListener('DOMContentLoaded', () => {
  //   const micButton = document.getElementById('control-button')
  const waversurferDiv = document.getElementById('wavesurfer')

  if (wavesurfer === undefined) {
    wavesurfer = WaveSurfer.create({
      container: '#wavesurfer',
      responsive: true,
      height: 300,
      barMinHeight: 2,
      barHeight: 1.1,
      waveColor: '#38b6ff',
      interact: false,
      barWidth: 6,
      cursorWidth: 0,
      barRadius: Math.PI,
      plugins: [
        MicrophonePlugin.create({
          bufferSize: 4096,
          numberOfInputChannels: 1,
          numberOfOutputChannels: 1,
          constraints: {
            video: false,
            audio: true,
          },
        }),
      ],
    })
    wavesurfer.microphone.on('deviceReady', (stream) => console.log('ready', stream))
    wavesurfer.microphone.on('deviceError', (error) => console.log('ready', error))
    wavesurfer.microphone.start()
    micButton.style.visibility = 'hidden'
  } else {
    if (wavesurfer.microphone.active) {
      wavesurfer.microphone.stop()
      micButton.style.visibility = 'visible'
    } else {
      wavesurfer.microphone.start()
      micButton.style.visibility = 'hidden'
    }
  }
})
