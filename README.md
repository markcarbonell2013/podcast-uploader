# Podcast Uploader

A repository to upload our podcasts to different platforms

# Usage

The index.html file is located on the **/view** directory. Together with the corresponding CSS and **bundle.js** file

1. Chrome
For the view to work properly, start chrome with

```bash
google-chrome --disable-web-security
```

2. Install npm modules

```bash
npm install
```

3. HTTP Server
In case http-server it's not installed yet do
```bash
sudo apt install http-server
```

Then just run start an http-server in the repo's base directory (like http-server)
```bash
cd <podcast-uploader-repo>
http-server
```

4. Browserify
The application uses browserify to bundle up a **bundle.js** file located in the **/view** directory. To do this you can either

```bash
cd <podcast-uploader-repo>
browserify view/main.js -o view/bundle.js
```

or execute the **update.sh** script with `sh update.sh`

5. Font

Install Norwester Font on your system before opening the browser



