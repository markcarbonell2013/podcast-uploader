#! /usr/bin/env bash

# This script takes as arguments:
# $1 (REQUIRED) the name of the .mp3 file you want to update
# $2 (OPTIONAL) --download flag in case you only want to download an already updated file. This will now upload a new file to Auphonic

COUNT=0
LIMIT=100
USERNAME="Weststadt_Podcast"
PASSWORD="zmHA362eQ2HZ*Qw"
PODCAST_UPLOADER_PATH="/home/neo/Documents/podcast/podcast-uploader"

if [ -z "$1" ]; then 
    echo "ERROR: Not input .mp3 file given...Exiting"
    exit 1
fi

PODCAST_FILE="$1"
PODCAST_FILENAME="$(echo "$1" | sed -En 's|.*/(.*)$|\1|p' )"
PODCAST_NEW_FILE_FILENAME="$(echo "$PODCAST_FILENAME" | sed -E "s/\.mp3/.aphonic.mp3/")"

if [ -z "$2" ]; then
    NEW_PODCAST_UUID="$(curl -X POST https://auphonic.com/api/simple/productions.json \
        -u $USERNAME:$PASSWORD \
        -F "preset=tyyU56ivEpVUzCqLwU6oth" \
        -F "title=Weststadt Podcast $(date)" \
        -F "genre=Podcast" \
        -F "input_file=@$PODCAST_UPLOADER_PATH/$PODCAST_FILE" \
        -F "action=start" | sed -En 's/ +"uuid": "(.*)",/\1/p')"
fi

if [ -z "$NEW_PODCAST_UUID" ] && [ -n "$2" ]; then
    NEW_PODCAST_UUID="$2"
fi

if [ -n "$NEW_PODCAST_UUID" ]; then 
    while true; do
        sleep 1
        curl --fail -X GET  \
            -o "$PODCAST_UPLOADER_PATH/media/$PODCAST_NEW_FILE_FILENAME" \
            "https://auphonic.com/api/download/audio-result/$NEW_PODCAST_UUID/$PODCAST_FILENAME" \
            -u "$USERNAME:$PASSWORD" && break
        if [ $COUNT -ge $LIMIT ];
        then 
            echo "WARNING: Retry limit reached. Exiting out of loop"
            break
        fi
    done
else 
    echo "ERROR: The file at $PODCAST_UPLOADER_PATH/media/$PODCAST_NEW_FILE_FILENAME
    could not be downloaded. Please check Auphonics website https://auphonic.com/engine/ for details"
    exit 1
fi










